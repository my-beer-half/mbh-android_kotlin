package com.avanwalre.mybeerhalf.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.avanwalre.mybeerhalf.R
import com.avanwalre.mybeerhalf.dto.Beer
import com.squareup.picasso.Picasso

class MatchersBeersAdapter(private var beerList: MutableList<Beer>?) : RecyclerView.Adapter<MatchersBeersAdapter.MyViewHolder>() {

    fun updateData(data: MutableList<Beer>) {
        beerList = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.matches_detail_layout, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val beerItem: Beer = beerList!![position]

        holder.tagline.text = beerItem.tagline
        holder.beerName.text = beerItem.name
        holder.description.text = beerItem.description
        holder.abv.text = String.format("%s%% ABV", beerItem.abv!!.toString())
        Picasso.get().load(beerItem.imageUrl).placeholder(R.mipmap.logo_hi_res_512).into(holder.detailImg)
    }

    override fun getItemCount(): Int {
        return beerList!!.size
    }

    inner class MyViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val abv: TextView = itemView.findViewById(R.id.matches_detail_label_abv)
        internal val beerName: TextView = itemView.findViewById(R.id.matches_detail_label_beername)
        internal val tagline: TextView = itemView.findViewById(R.id.matches_detail_label_tagline)
        internal val description: TextView = itemView.findViewById(R.id.matches_detail_label_description)
        internal val detailImg: ImageView = itemView.findViewById(R.id.matches_detail_image)
    }
}