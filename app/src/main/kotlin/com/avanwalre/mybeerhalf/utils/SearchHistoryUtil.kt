package com.avanwalre.mybeerhalf.utils

import android.util.Log

import com.avanwalre.mybeerhalf.database.AppDatabase
import com.avanwalre.mybeerhalf.entities.SearchHistory

object SearchHistoryUtil {

    fun getSearchHistory(database: AppDatabase?, foodName: String?): SearchHistory? {
        val tag =  "getSearchHistory@" + SearchHistoryUtil::class.java.simpleName
        Log.i(tag, "Getting search history data for food '$foodName'")
        return database?.searchHistoryDao()?.findByFoodName(foodName)
    }

    fun insertSearchHistory(database: AppDatabase?, foodName: String?, jsonData: String) {
        val tag = "@insertSearchHistory@" + SearchHistoryUtil::class.java.simpleName
        Log.i(tag, "Inserting new search history data for food '$foodName'")
        database?.searchHistoryDao()?.insert(SearchHistory().foodName(foodName).jsonData(jsonData))
    }

}
