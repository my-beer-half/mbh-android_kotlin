package com.avanwalre.mybeerhalf.database

import android.content.Context

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import com.avanwalre.mybeerhalf.dao.SearchHistoryDao
import com.avanwalre.mybeerhalf.entities.SearchHistory

@Database(entities = [SearchHistory::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun searchHistoryDao(): SearchHistoryDao

    companion object {

        private var INSTANCE: AppDatabase? = null
        private const val DATABASE_NAME = "mybeerhalf_db"

        fun getAppDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DATABASE_NAME).allowMainThreadQueries().build()
            }

            return INSTANCE
        }
    }

}
