package com.avanwalre.mybeerhalf.activities

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.avanwalre.mybeerhalf.R
import com.avanwalre.mybeerhalf.adapters.MatchersBeersAdapter
import com.avanwalre.mybeerhalf.apis.ApiInvoker
import com.avanwalre.mybeerhalf.database.AppDatabase
import com.avanwalre.mybeerhalf.dto.Beer
import com.avanwalre.mybeerhalf.enums.Extras
import com.avanwalre.mybeerhalf.utils.SearchHistoryUtil
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException
import kotlin.Comparator

class MatchesActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private var recyclerViewAdapter: MatchersBeersAdapter? = null
    private var isAbvIncOrder = true
    private var beerList: MutableList<Beer>? = null
    private var menu: Menu? = null
    private var progressBar: ProgressBar? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var layoutNoResults: LinearLayout? = null
    private var isErrorOn = false
    private var foodName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.matches_layout)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressBar = findViewById(R.id.matches_progressBar)
        layoutNoResults = findViewById(R.id.matches_layout_noresult)

        // Init
        initSwipeRefreshLayout()
        initBeerRecyclerView()

        // First checks if there's offline data
        loadData()
    }

    /**
     * Decides if data is needed to be collected from the Internet.
     */
    private fun loadData() {
        val TAG = this.javaClass.simpleName + "@loadData"

        foodName = this.intent.getStringExtra(Extras.EXTRA_FOOD.key())!!.replace(" ", "_")

        Log.i(TAG, "Init for food name '$foodName'")

        val searchHistory = SearchHistoryUtil.getSearchHistory(AppDatabase.getAppDatabase(this), foodName)

        if (searchHistory != null) {
            loadJsonData(searchHistory.jsonData)
        } else {
            callApi()
        }
    }

    /**
     * Calls API for obtaining data.
     */
    private fun callApi() {
        val TAG = this.javaClass.simpleName + "@callApi"

        Log.i(TAG, "Getting online data for food name '$foodName'")

        val apiInvoker = ApiInvoker.getInstance(this)
        val path = "beers?food=" + foodName!!
        val url = HOST + path

        // Request
        val stringRequest = StringRequest(
                Request.Method.GET,
                url,
                Response.Listener { response ->
                    if (response != "[]") {
                        saveToDatabase(response)
                    }
                    loadJsonData(response)
                },
                Response.ErrorListener { error ->
                    Log.e(TAG, "Error while getting data: " + error.message)
                    toggleProgressBar()
                    showError(true)
                }
        )

        // Invoke request
        apiInvoker!!.addToRequestQueue(stringRequest)
    }

    /**
     * Saves data to database for future requests.
     *
     * @param jsonData beer's json data
     */
    private fun saveToDatabase(jsonData: String) {
        SearchHistoryUtil.insertSearchHistory(AppDatabase.getAppDatabase(this), foodName, jsonData)
    }

    /**
     * Processes the obtained data.
     *
     * @param response data response
     */
    private fun loadJsonData(response: String?) {
        val TAG = this.javaClass.simpleName + "@loadJsonData"

        try {
            beerList = mutableListOf(*ObjectMapper().readValue<Array<Beer>>(response, Array<Beer>::class.java))

            if (beerList!!.isNotEmpty()) {
                orderByAbv()
                recyclerViewAdapter = MatchersBeersAdapter(beerList)
                recyclerView.adapter = recyclerViewAdapter
            } else {
                showError(false)
            }

            toggleProgressBar()
            swipeRefreshLayout!!.isRefreshing = false
        } catch (e: IOException) {
            Log.e(TAG, "Error while parsing data: " + e.message)
        }

    }

    /**
     * Show message error in the middle of the screen.
     *
     * @param isConnectionFailure indicates if there's an error with the Internet connection
     */
    private fun showError(isConnectionFailure: Boolean) {
        isErrorOn = true

        if (isConnectionFailure) {
            val errorLabel = findViewById<TextView>(R.id.matches_noresult)
            errorLabel.setText(R.string.matches_label_noconnection)
        }

        checkHidingActionButton()

        layoutNoResults!!.visibility = View.VISIBLE
    }

    /**
     * Initialize beer recycler view.
     */
    private fun initBeerRecyclerView() {
        recyclerView = findViewById(R.id.matches_rv_beers)
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)
    }

    /**
     * Initialize beer swipe refresh layout.
     */
    private fun initSwipeRefreshLayout() {
        swipeRefreshLayout = findViewById(R.id.matches_swipeRefresh)
        swipeRefreshLayout!!.setOnRefreshListener {
            toggleProgressBar()
            loadData()
        }
    }

    /**
     * Inc/dec order by ABV property.
     */
    private fun orderByAbv() {
        beerList!!.sortWith(Comparator { beer1, beer2 ->
            when (isAbvIncOrder) {
                true -> beer1.abv!!.compareTo(beer2.abv!!)
                false -> beer2.abv!!.compareTo(beer1.abv!!)
            }
        })

        recyclerViewAdapter?.updateData(beerList!!)
    }

    /**
     * Changes drawable and text from ordering icon.
     */
    private fun toggleOrderingIcon() {
        if (isAbvIncOrder) {
            menu!!.findItem(R.id.menu_top_matches_orderAbv).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_abv_dec)).setTitle(R.string.matches_actionbutton_order_inc)
        } else {
            menu!!.findItem(R.id.menu_top_matches_orderAbv).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_abv_inc)).setTitle(R.string.matches_actionbutton_order_dec)
        }
    }

    /**
     * Toggle progress bar visibility.
     */
    private fun toggleProgressBar() {
        progressBar!!.visibility = if (progressBar!!.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    /**
     * Hides action buttton in case of error.
     */
    private fun checkHidingActionButton() {
        if (menu != null && isErrorOn) {
            menu!!.findItem(R.id.menu_top_matches_orderAbv).isVisible = false
        }
    }

    /**
     * Action bar buttons.
     *
     * @param item item selected
     * @return return if the item one is selected
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            R.id.menu_top_matches_orderAbv -> {
                isAbvIncOrder = !isAbvIncOrder
                orderByAbv()
                toggleOrderingIcon()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Inflate top menu.
     *
     * @param menu menu to be inflated
     * @return menu
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu

        menuInflater.inflate(R.menu.menu_top_matches, menu)

        checkHidingActionButton()

        return super.onCreateOptionsMenu(menu)
    }

    companion object {

        // Constants
        private const val HOST = "https://api.punkapi.com/v2/"
    }

}
