package com.avanwalre.mybeerhalf.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.avanwalre.mybeerhalf.R
import com.avanwalre.mybeerhalf.enums.Extras

class MainActivity : Activity() {

    private var fieldFood: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)

        val buttonSearch = findViewById<Button>(R.id.main_button_search)
        fieldFood = findViewById(R.id.main_field_food)

        buttonSearch.setOnClickListener {
            val foodData = fieldFood!!.text.toString()

            if (foodData.isNotEmpty()) {
                val intent = Intent(applicationContext, MatchesActivity::class.java)
                intent.putExtra(Extras.EXTRA_FOOD.key(), fieldFood!!.text.toString())
                startActivity(intent)
            }
        }
    }

}
