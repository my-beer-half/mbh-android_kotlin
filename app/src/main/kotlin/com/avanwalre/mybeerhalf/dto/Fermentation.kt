package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("temp")
class Fermentation {

    @JsonProperty("temp")
    @get:JsonProperty("temp")
    @set:JsonProperty("temp")
    var temp: Temp_? = null

}
