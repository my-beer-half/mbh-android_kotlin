package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("mash_temp", "fermentation", "twist")
class Method {

    @JsonProperty("mash_temp")
    @get:JsonProperty("mash_temp")
    @set:JsonProperty("mash_temp")
    var mashTemp: List<MashTemp>? = null
    @JsonProperty("fermentation")
    @get:JsonProperty("fermentation")
    @set:JsonProperty("fermentation")
    var fermentation: Fermentation? = null
    @JsonProperty("twist")
    @get:JsonProperty("twist")
    @set:JsonProperty("twist")
    var twist: Any? = null

}
