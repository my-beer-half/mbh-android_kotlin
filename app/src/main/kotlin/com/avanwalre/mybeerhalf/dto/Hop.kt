package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("name", "amount", "add", "attribute")
class Hop {

    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null
    @JsonProperty("amount")
    @get:JsonProperty("amount")
    @set:JsonProperty("amount")
    var amount: Amount_? = null
    @JsonProperty("add")
    @get:JsonProperty("add")
    @set:JsonProperty("add")
    var add: String? = null
    @JsonProperty("attribute")
    @get:JsonProperty("attribute")
    @set:JsonProperty("attribute")
    var attribute: String? = null

}
