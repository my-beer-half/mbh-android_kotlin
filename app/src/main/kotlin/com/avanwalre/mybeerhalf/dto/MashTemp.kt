package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("temp", "duration")
class MashTemp {

    @JsonProperty("temp")
    @get:JsonProperty("temp")
    @set:JsonProperty("temp")
    var temp: Temp? = null
    @JsonProperty("duration")
    @get:JsonProperty("duration")
    @set:JsonProperty("duration")
    var duration: Int? = null

}
