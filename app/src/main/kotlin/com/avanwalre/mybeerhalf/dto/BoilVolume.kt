package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("value", "unit")
class BoilVolume {

    @JsonProperty("value")
    @get:JsonProperty("value")
    @set:JsonProperty("value")
    var value: Int? = null
    @JsonProperty("unit")
    @get:JsonProperty("unit")
    @set:JsonProperty("unit")
    var unit: String? = null

}
