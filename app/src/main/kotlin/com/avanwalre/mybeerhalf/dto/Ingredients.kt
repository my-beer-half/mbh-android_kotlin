package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("malt", "hops", "yeast")
class Ingredients {

    @JsonProperty("malt")
    @get:JsonProperty("malt")
    @set:JsonProperty("malt")
    var malt: List<Malt>? = null
    @JsonProperty("hops")
    @get:JsonProperty("hops")
    @set:JsonProperty("hops")
    var hops: List<Hop>? = null
    @JsonProperty("yeast")
    @get:JsonProperty("yeast")
    @set:JsonProperty("yeast")
    var yeast: String? = null

}
