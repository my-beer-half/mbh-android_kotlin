package com.avanwalre.mybeerhalf.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("id", "name", "tagline", "first_brewed", "description", "image_url", "abv", "ibu", "target_fg", "target_og", "ebc", "srm", "ph", "attenuation_level", "volume", "boil_volume", "method", "ingredients", "food_pairing", "brewers_tips", "contributed_by")
class Beer {

    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Int? = null
    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null
    @JsonProperty("tagline")
    @get:JsonProperty("tagline")
    @set:JsonProperty("tagline")
    var tagline: String? = null
    @JsonProperty("first_brewed")
    @get:JsonProperty("first_brewed")
    @set:JsonProperty("first_brewed")
    var firstBrewed: String? = null
    @JsonProperty("description")
    @get:JsonProperty("description")
    @set:JsonProperty("description")
    var description: String? = null
    @JsonProperty("image_url")
    @get:JsonProperty("image_url")
    @set:JsonProperty("image_url")
    var imageUrl: String? = null
    @JsonProperty("abv")
    @get:JsonProperty("abv")
    @set:JsonProperty("abv")
    var abv: Double? = null
    @JsonProperty("ibu")
    @get:JsonProperty("ibu")
    @set:JsonProperty("ibu")
    var ibu: Int? = null
    @JsonProperty("target_fg")
    @get:JsonProperty("target_fg")
    @set:JsonProperty("target_fg")
    var targetFg: Int? = null
    @JsonProperty("target_og")
    @get:JsonProperty("target_og")
    @set:JsonProperty("target_og")
    var targetOg: Int? = null
    @JsonProperty("ebc")
    @get:JsonProperty("ebc")
    @set:JsonProperty("ebc")
    var ebc: Int? = null
    @JsonProperty("srm")
    @get:JsonProperty("srm")
    @set:JsonProperty("srm")
    var srm: Double? = null
    @JsonProperty("ph")
    @get:JsonProperty("ph")
    @set:JsonProperty("ph")
    var ph: Double? = null
    @JsonProperty("attenuation_level")
    @get:JsonProperty("attenuation_level")
    @set:JsonProperty("attenuation_level")
    var attenuationLevel: Double? = null
    @JsonProperty("volume")
    @get:JsonProperty("volume")
    @set:JsonProperty("volume")
    var volume: Volume? = null
    @JsonProperty("boil_volume")
    @get:JsonProperty("boil_volume")
    @set:JsonProperty("boil_volume")
    var boilVolume: BoilVolume? = null
    @JsonProperty("method")
    @get:JsonProperty("method")
    @set:JsonProperty("method")
    var method: Method? = null
    @JsonProperty("ingredients")
    @get:JsonProperty("ingredients")
    @set:JsonProperty("ingredients")
    var ingredients: Ingredients? = null
    @JsonProperty("food_pairing")
    @get:JsonProperty("food_pairing")
    @set:JsonProperty("food_pairing")
    var foodPairing: List<String>? = null
    @JsonProperty("brewers_tips")
    @get:JsonProperty("brewers_tips")
    @set:JsonProperty("brewers_tips")
    var brewersTips: String? = null
    @JsonProperty("contributed_by")
    @get:JsonProperty("contributed_by")
    @set:JsonProperty("contributed_by")
    var contributedBy: String? = null

}
