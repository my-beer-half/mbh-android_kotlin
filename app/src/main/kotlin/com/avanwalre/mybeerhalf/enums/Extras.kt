package com.avanwalre.mybeerhalf.enums

enum class Extras(private val key: String) {
    EXTRA_FOOD("EXTRA_FOOD");

    fun key(): String {
        return key
    }
}
