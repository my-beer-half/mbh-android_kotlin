package com.avanwalre.mybeerhalf.apis

import android.content.Context

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley


class ApiInvoker private constructor(private val mContext: Context) {
    private var mRequestQueue: RequestQueue? = null

    private// If RequestQueue is null the initialize new RequestQueue
    // Return RequestQueue
    val requestQueue: RequestQueue?
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(mContext.applicationContext)
            }
            return mRequestQueue
        }

    init {
        // Get the request queue
        mRequestQueue = requestQueue
    }// Specify the application context

    fun <T> addToRequestQueue(request: Request<T>) {
        // Add the specified request to the request queue
        requestQueue?.add(request)
    }

    companion object {

        private var mInstance: ApiInvoker? = null

        @Synchronized
        fun getInstance(context: Context): ApiInvoker? {
            // If Instance is null then initialize new Instance
            if (mInstance == null) {
                mInstance = ApiInvoker(context)
            }
            // Return MySingleton new Instance
            return mInstance
        }
    }
}

