package com.avanwalre.mybeerhalf.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_history")
class SearchHistory {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "food_name")
    var foodName: String? = null

    @ColumnInfo(name = "json_data")
    var jsonData: String? = null

    fun id(id: Int): SearchHistory {
        this.id = id
        return this
    }

    fun foodName(foodName: String?): SearchHistory {
        this.foodName = foodName
        return this
    }

    fun jsonData(jsonData: String): SearchHistory {
        this.jsonData = jsonData
        return this
    }


}
