package com.avanwalre.mybeerhalf

import androidx.test.espresso.intent.Intents
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule

import com.avanwalre.mybeerhalf.activities.MainActivity
import com.avanwalre.mybeerhalf.activities.MatchesActivity
import com.avanwalre.mybeerhalf.utils.RecyclerViewItemCountAssertion

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import java.util.Random

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.core.AllOf.allOf

/**
 * ATTENTION! Before running these tests, it is mandatory to keep data connection OFF!
 */
class MatchesActivityOfflineEspressoTest {

    @Rule @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun writeFoodAndObtainData() {
        val FOOD_NAME = "fish"
        val NUMBER_OF_RESULTS = 18

        Intents.init()

        try {
            onView(withId(R.id.main_field_food)).perform(typeText(FOOD_NAME), closeSoftKeyboard())
            onView(withId(R.id.main_button_search)).perform(click())

            intended(hasComponent(MatchesActivity::class.java.name))

            onView(withId(R.id.matches_rv_beers)).check(RecyclerViewItemCountAssertion(NUMBER_OF_RESULTS))
        } finally {
            Intents.release()
        }
    }

    @Test
    fun writeFoodAndNotObtainData() {
        val FOOD_NAME = "patates braves" + Random().nextInt()

        Intents.init()

        try {
            onView(withId(R.id.main_field_food)).perform(typeText(FOOD_NAME), closeSoftKeyboard())
            onView(withId(R.id.main_button_search)).perform(click())

            intended(hasComponent(MatchesActivity::class.java.name))

            onView(allOf(withId(R.id.matches_noresult), withText(R.string.matches_label_noconnection))).check(matches(isDisplayed()))
        } finally {
            Intents.release()
        }
    }


}