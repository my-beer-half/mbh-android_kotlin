package com.avanwalre.mybeerhalf

import android.content.Context

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4

import com.avanwalre.mybeerhalf.dao.SearchHistoryDao
import com.avanwalre.mybeerhalf.database.AppDatabase
import com.avanwalre.mybeerhalf.entities.SearchHistory

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import java.io.IOException

import org.junit.Assert.*

@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private var searchHistoryDao: SearchHistoryDao? = null
    private var db: AppDatabase? = null

    private val FOOD_NAME = "omelette"

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        searchHistoryDao = db!!.searchHistoryDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db!!.close()
    }

    @Test
    fun insertAndGetRecordEqual() {
        searchHistoryDao!!.insert(SearchHistory().foodName(FOOD_NAME).jsonData("this_is_an_example"))
        val searchHistoryResult = searchHistoryDao!!.findByFoodName(FOOD_NAME)
        assertNotNull(searchHistoryResult)
    }

    @Test
    fun insertAndGetRecordNonEqual() {
        val searchHistoryResult = searchHistoryDao!!.findByFoodName(FOOD_NAME + "_")
        assertNull(searchHistoryResult)
    }

}
