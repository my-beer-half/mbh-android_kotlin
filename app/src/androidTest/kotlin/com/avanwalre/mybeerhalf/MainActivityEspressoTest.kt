package com.avanwalre.mybeerhalf

import androidx.test.espresso.intent.Intents
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule

import com.avanwalre.mybeerhalf.activities.MainActivity
import com.avanwalre.mybeerhalf.activities.MatchesActivity

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.withId


class MainActivityEspressoTest {

    @Rule @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun writeAndLaunchMatchesActivity() {
        Intents.init()

        onView(withId(R.id.main_field_food))
                .perform(typeText("potato"), closeSoftKeyboard())

        onView(withId(R.id.main_button_search)).perform(click())

        intended(hasComponent(MatchesActivity::class.java.name))

        Intents.release()
    }

    @Test
    fun writeNothingAndStayAtSameActivity() {
        onView(withId(R.id.main_field_food))
                .perform(typeText(""), closeSoftKeyboard())

        onView(withId(R.id.main_button_search)).perform(click())

        hasComponent(MainActivity::class.java.name)
    }

}